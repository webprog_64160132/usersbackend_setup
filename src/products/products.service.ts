import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Product 1', price: 200 },
  { id: 2, name: 'Product 2', price: 300 },
  { id: 3, name: 'Product 3', price: 400 },
];

let lastUserId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastUserId++,
      ...createProductDto, //name, price
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((products) => {
      return products.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((products) => {
      return products.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const delatedProduct = products[index];
    products.splice(index, 1);
    return delatedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'Product 1', price: 200 },
      { id: 2, name: 'Product 2', price: 300 },
      { id: 3, name: 'Product 3', price: 400 },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
